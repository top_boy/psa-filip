from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import GridSearchCV
from sklearn.feature_selection import RFECV
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn.cluster import KMeans as KM
from sklearn import manifold
from tsfresh.transformers import PerColumnImputer as PCI
from tsfresh import select_features
from multiprocessing import Process, Queue
from mio.initialDesigns import latinHypercubeSampling as lhs
from mio.sampling import maximinSampling as mms
from mio.featureExtraction import timeSeriesAnalysis as tsa
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy
import gillespy
import time
import sys


class WorkFlow():

    def __init__(self, ts_analysis, max_, min_, model_run, set_params, timeout=50):
        self.ts_analysis = ts_analysis
        self.estimators = []
        self.selected_features = []
        self.fc_params = []
        self.computed_samples = []
        self.exploded_samples = []
        self.all_samples = []
        self.max_ = max_
        self.min_ = min_
        self.sampling = mms.MaximinSampling(xmin=min_, xmax=max_)
        self.run = model_run
        self.set_params = set_params
        self.data = [] 
        self.minmax_scaler = None
        self.timeout = timeout
        self.runtime = []
        self.batch_idx = [0,0]
        self.batch_history = []

    def sample(self, N=50):
        sys.stdout.write("Sampling %s candidates\n" % N)
        if not self.all_samples:
            self.candidates = self.sampling.selectPoints(numpy.array([self.max_, self.min_]), N)
        else:
            self.candidates = self.sampling.selectPoints(numpy.asarray(self.all_samples), N)
        sys.stdout.write("Sampling done!\n")

    def simulator(self, params):
        q = Queue()
        p = Process(target=self.run, name="Simulator", args=(q,))
        t_start = time.time()
        p.start()
        p.join(self.timeout)
        t_end = time.time()
        if p.is_alive():
            p.terminate()
            self.exploded_samples.append(params)
            self.all_samples.append(params)
            p.join()
        else:
            p.join()
            self.data.append(q.get())
            self.runtime.append(t_end-t_start)
            self.computed_samples.append(params)
            self.all_samples.append(params)

    def simulate_candidates(self):
        if type(self.data) is numpy.ndarray:
            self.data = self.data.tolist()
        c = len(self.data)
        sys.stdout.write("Starting simulating candidates...\n")
        for e, params in enumerate(self.candidates):
            self.set_params(params)
            self.simulator(params)
            # TODO: change nr_exploaded to current batch 
            sys.stdout.write("\r%d%% progress (nr exploaded samples: %d)" % ((float(e)/(len(self.candidates) -1))*100, len(self.exploded_samples)))
            sys.stdout.flush()
        c_after = len(self.data)
        self.ts_analysis.put(self.data[c - c_after:])
        self.batch_idx[1] = c_after
        self.batch_history.append((len(self.candidates),c_after - c)) # (N, nr_computed_samples) 


    def generate_features(self):
        if not self.fc_params:
            self.ts_analysis.generate()
        else:
            self.ts_analysis.generate(fc_params = self.fc_params)


    def pre_process(self):
        #Impute
        self.batch = PCI().fit(self.ts_analysis.features).transform(self.ts_analysis.features)
        self.batch = self.batch.as_matrix()[self.batch_idx[0]:self.batch_idx[1]]
        #Normalization
        if self.estimators:
            self.batch = self.minmax_scaler.transform(self.batch)
        else:
            self.minmax_scaler = MinMaxScaler()
            self.minmax_scaler.fit(self.batch)
            self.batch = self.minmax_scaler.transform(self.batch)



    def visualize(self, skip_KM = False, title = "test", nr_clusters=10):
            #Dimension reduction to 2D using t-SNE
            tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
            X_tsne = tsne.fit_transform(self.batch)

            if not skip_KM:
                #Clustering of 2D data from t-SNE
                km = KM(n_clusters=nr_clusters, init='random' )
                km.fit(X_tsne)
                self.labels = km.labels_
                cluster_centers = km.cluster_centers_

            self.data = numpy.asarray(self.data)

            self.interactive_plot(X_tsne, range(len(X_tsne)), self.labels, title)


    def get_user_feedback(self):
            user_feedback = raw_input("Type preferred clusters: ")
            user_feedback = [int(cluster) for cluster in user_feedback.split()]

            for e, val in enumerate(self.labels):
                if val in user_feedback:
                    self.labels[e] = 1
                else:
                    self.labels[e] = 0


    def learn_classifier(self):
        print('starting learning the first classifier/predictor...this will take a few minutes')
        param_grid = [{'estimator__C': [0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]},{'estimator__kernel': ['linear']}]
        estimator = SVC(kernel='linear')
        rfecv = RFECV(estimator=estimator, step=1,scoring='accuracy')
        clf = GridSearchCV(rfecv, param_grid,scoring='accuracy')
        clf.fit(self.batch, self.labels)
        print "Optimal number of features : %d" % clf.best_estimator_.n_features_
        self.estimators.append(clf.best_estimator_)
        self.selected_features = clf.best_estimator_.get_support(indices=True).tolist()
        self.fc_params = self.ts_analysis.get_fc_params(self.selected_features)

        # Plot number of features VS. cross-validation scores
        plt.figure()
        plt.xlabel("Number of features selected")
        plt.ylabel("Cross validation score (nr of correct classifications)")
        plt.plot(range(1, len(clf.best_estimator_.grid_scores_) + 1),clf.best_estimator_.grid_scores_)
        plt.show()

    def predict(self, estimator, data):
        return estimator.estimator_.predict(data)




    def interactive_plot(self, data,indices,labels,title):
        def onpick3(event):

                ind = event.ind
                text.set_position((data[ind,0], data[ind,1]))
                text.set_text(str(ind) + str(labels[ind]))


                #ts = ts[ind]
                ts = ts_data[ind].T[-1] #Activator protein
                t = ts_data[ind].T[0] #timepoints

                maxi = str(int(max(ts)[0]))
                mini = str(int(min(ts)[0]))

                ax2.set_xlim(0,max(t))
                ax2.set_ylim(0,1.15)
                inc = max(t)/10
                ax2.set_xticks(numpy.arange(min(t),max(t)+inc, inc))


                ts = (ts - min(ts))/float(max(ts) - min(ts))
                ax2.set_yticks(numpy.arange(min(ts), max(ts)+.1, 0.5))

                time_s.set_visible(True)
                time_s.set_ydata(ts)
                time_s.set_xdata(t)


                arg_max =numpy.argmax(ts)
                arg_min = numpy.argmin(ts)


                max_plot.set_ydata(ts[arg_max])
                max_plot.set_xdata(t[arg_max])
                max_plot.set_label('Max: '+maxi)


                ax2.legend(loc='upper right', shadow=True)



                fig.canvas.draw()
                #print 'onpick3 scatter:', ind, indices[ind],labels[ind], np.take(data[:, 0], ind), np.take(data[:, 1], ind)

        N = len(numpy.unique(labels))
        ts_data = self.data[self.batch_idx[0]:self.batch_idx[1]]

        # define the colormap
        cmap = plt.cm.jet
        # extract all colors from the .jet map
        cmaplist = [cmap(i) for i in range(cmap.N)]
        # create the new map
        cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)

        # define the bins and normalize
        bounds = numpy.linspace(0,N,N+1)
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)


        test = ts_data[0].T[-1]

        fig, (ax,ax2) = plt.subplots(1, 2, figsize=(12.5,5), gridspec_kw = {'width_ratios':[1, 1.3]})
        text = ax.text(0, 0, "", va="bottom", ha="left")
        text.set_text('test')


        line = ax.scatter(data[:, 0], data[:, 1], picker=True, c=labels,cmap=cmap,norm=norm)
        time_s,  = ax2.plot(test, visible=True, alpha=0.7)

        max_plot, = ax2.plot(0,0,'ro', markersize= 12, label='Max')


        ax.set_xlabel("tsne 1", style='italic', size=20)
        ax.set_ylabel("tsne 2",style='italic', size=20)
        ax2.set_xlabel("time (hours)", style='italic', size=20)
        ax2.set_ylabel("copy number",style='italic', size=20)

        ax2.tick_params(labelsize=12)
        ax.tick_params(labelsize=12)
        max_data = int(max(data[:, 0]))
        min_data = int(min(data[:, 0]))
        ax.set_xticks(numpy.arange(min_data,max_data + 1, (max_data - min_data)/5))

        max_data = int(max(data[:, 1]))
        min_data = int(min(data[:, 1]))
        ax.set_yticks(numpy.arange(min_data,max_data + 1, (max_data - min_data)/5))

        fig.canvas.mpl_connect('pick_event', onpick3)

        plt.show()
